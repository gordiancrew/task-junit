import java.util.Arrays;

import static javax.management.Query.plus;

public class App {

    public static void main(String[] args) throws QuadraticEquationException {

        System.out.println("Roots: "+Arrays.toString(QuadraticEquation.calculRoots(1,-6,9)));
    }
}

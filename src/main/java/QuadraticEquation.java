import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class QuadraticEquation {

    public static double[] calculRoots(double a, double b, double c) throws QuadraticEquationException {

        double[] res;
        double D = pow(b, 2) - 4 * a * c;
        if (a == 0) throw new QuadraticEquationException("Error");
        if (D > 0) {
            return new double[]{((-b + sqrt(D)) / (2 * a)), ((-b - sqrt(D)) / (2 * a))};
        } else if (D == 0) {
            return new double[]{(-b / (2 * a))};
        } else {
            return new double[]{};
        }
    }
}

class QuadraticEquationException extends Exception {

    public QuadraticEquationException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}

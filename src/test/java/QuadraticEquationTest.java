
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class QuadraticEquationTest {

    @Test
    public void TwoRootsTest() throws QuadraticEquationException {
        double[] actual = QuadraticEquation.calculRoots(4, 5, 1);
        double[] expected = {-0.25, -1};
        assertTrue(Arrays.equals(expected, actual));
    }

    @Test
    public void OneRootTest() throws QuadraticEquationException {
        double[] actual = QuadraticEquation.calculRoots(1, -6, 9);
        assertTrue(actual.length == 1);
    }

    @Test
    public void NotRootsTest() throws QuadraticEquationException {
        double[] actual = QuadraticEquation.calculRoots(100, 2, 2);
        assertTrue(actual.length == 0);
    }

    @Test
    public void AZeroTest() throws QuadraticEquationException {
        try {
            double[] roots = QuadraticEquation.calculRoots(0, 5, 1);
        } catch (QuadraticEquationException e) {
            assertEquals(e.getClass(), QuadraticEquationException.class);
        }
    }

    @Test
    public void ABCZeroTest() throws QuadraticEquationException {
        try {
            double[] roots = QuadraticEquation.calculRoots(0, 0, 0);
        } catch (QuadraticEquationException e) {
            assertEquals("Error", e.getMessage());
        }
    }

    @Test
    public void BCZeroTest() throws QuadraticEquationException {
        double[] actual = QuadraticEquation.calculRoots(2, 0, 0);
        assertTrue(actual[0] == 0);
    }

    @Test
    public void CZeroTest() throws QuadraticEquationException {
        double[] roots = QuadraticEquation.calculRoots(2, 2, 0);
        boolean ok = false;
        for (double root : roots) {
            if (root == 0)
                ok = true;
        }
        assertTrue(ok);
    }

    @Test
    public void BZeroTest() throws QuadraticEquationException {
        double[] actual = QuadraticEquation.calculRoots(2, 0, 2);
        assertTrue(actual.length != 1);
    }
}
